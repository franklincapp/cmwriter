#!/usr/bin/env python3
import sys, traceback
import pandas as pd

__author__ = "Franklin Cappadora"
__version__ = "0.1.0"
__license__ = "MIT"


def main(cmType, file):
	cmData = pd.read_csv(file).dropna(how='all', axis='columns')

	for i in range(len(cmData)):
		fName = "./output/" + str(cmType) + '.' + cmData.loc[i,'DeveloperName'] + ".md-meta.xml"
		f = open(fName,"w")
		f.write('<?xml version="1.0" encoding="UTF-8"?>\n')
		f.write('<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">\n')
		f.write('\t<label>')
		f.write(cmData["Label"][i])
		f.write('</label>\n\t<protected>false</protected>')

		remainingData = cmData.drop(columns=['DeveloperName','Label'])
		fieldCols = remainingData.columns
		for j in range(len(fieldCols)):
			f.write('\n\t<values>\n\t\t<field>')
			f.write(fieldCols[j])
			if (str(remainingData.iloc[i,j]) == 'True' or str(remainingData.iloc[i,j]) == 'False'):
				f.write('</field>\n\t\t<value xsi:type="xsd:boolean">')
			else:
				f.write('</field>\n\t\t<value xsi:type="xsd:string">')
			if str(remainingData.iloc[i,j]) != "nan":
				f.write(str(remainingData.iloc[i,j]))
			f.write('</value>\n\t</values>')
		
		f.write('\n</CustomMetadata>')
		f.close()

if __name__ == "__main__":
	try:
		main(sys.argv[1], sys.argv[2])
	except Exception as err:
		print('~~~~~~~~~~~~~~~~~~~~~~~')
		print('Danger Will Robinson!')
		traceback.print_exc(file=sys.stdout)
		print('~~~~~~~~~~~~~~~~~~~~~~~')